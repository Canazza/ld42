﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour {
    public float Delay = 1;
    public UnityEvent OnTimeOut,OnTimeStart;
    private void OnEnable()
    {
        OnTimeStart.Invoke();
        Invoke("TimeOut", Delay);
    }
    void TimeOut()
    {
        OnTimeOut.Invoke();
    } 
}
