﻿using com.clydebuiltgames.Pooling;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WaveController : MonoBehaviour {
    public Spawner[] Spawners;
    public List<GameObject> Enemies;
    public UnityEvent OnWaveComplete, OnWaveStart,OnWaveSpwaned; 
    // Use this for initialization
    void OnEnable() {
        Spawners = this.GetComponentsInChildren<Spawner>();
    }
    public void StartWave()
    {
        StartCoroutine(DoWave());
    }
    IEnumerator DoWave()
    { 
        Debug.Log("WAVE STARTED");
        OnWaveStart.Invoke();
        Enemies = new List<GameObject>();
        foreach (var spawner in Spawners)
        {
            Enemies.Add(spawner.GetSpawn());
        }
        Debug.Log("WAVE SPAWNED");
        OnWaveSpwaned.Invoke();
        while (Enemies.Count(x => x.activeSelf) > 0)
        {
            yield return null;
        }
        Debug.Log("WAVE COMPLETE");
        OnWaveComplete.Invoke(); 
    } 
}
