﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WeaponPickup : MonoBehaviour ,ICollidable{
    public string WeaponName;
    public UnityEvent OnPickup;
    public SpriteRenderer Sprite;
    public float WaitBeforeFlash = 5;
    public float FlashDuration = 4;
    void OnEnable()
    {
        Sprite.enabled = true;
        StartCoroutine(DoDestroyTimer());
    }
    private IEnumerator DoDestroyTimer()
    {
        yield return new WaitForSeconds(WaitBeforeFlash);
        StartCoroutine(Flash());
        yield return new WaitForSeconds(FlashDuration);
        this.gameObject.SetActive(false);
    }
    private IEnumerator Flash()
    {
        while (true)
        {
            Sprite.enabled = true;
            yield return new WaitForSeconds(0.1f);
            Sprite.enabled = false;
            yield return new WaitForSeconds(0.1f);

        }
    }
    public void CollidedWith(GameObject source)
    {
        var weaponControl = source.GetComponentInParent<WeaponController>();
        if (weaponControl != null)
        {
            weaponControl.SelectWeapon(WeaponName);
            OnPickup.Invoke();
        }
    }
}
