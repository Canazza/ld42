﻿using com.clydebuiltgames.Controls;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TurretAI : MonoBehaviour {
    public Transform LookAtTarget;
    public LayerMask CanFireCastMask;
    public UnityEvent OnCanFire, OnCantFire;
    public float RotationSpeed = 1;
    public float FiringDistance = 6;
    private void Start()
    {
        if(LookAtTarget == null)
        {
            LookAtTarget = FindObjectOfType<TopDownController>().transform;
        }
    }
    private void OnDisable()
    {
        OnCantFire.Invoke();
    }
    // Update is called once per frame
    void Update () {
        var diff = LookAtTarget.position - this.transform.position ;
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0f, 0f, rot_z ),RotationSpeed * Time.deltaTime);
        if (Vector2.Distance(transform.position, LookAtTarget.position) < FiringDistance && !Physics.Linecast(this.transform.position,LookAtTarget.position,CanFireCastMask))
        { 
            OnCanFire.Invoke();
        } else
        { 
            OnCantFire.Invoke();
        }
	}
}

