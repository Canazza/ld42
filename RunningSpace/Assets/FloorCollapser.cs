﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FloorCollapser : MonoBehaviour {
    [System.Serializable]
    public class FloorBundle {
        public TilemapRenderer Floor, Abyss,Walls;
    }
    public List<FloorBundle> Bundles;
    public float TimeBetweenCollapses = 25;
    public float FlashTime = 5;
    private void Awake()
    {
        ResetFloors();
    }
    public void ResetFloors() {
        StopAllCoroutines();
        Bundles.ForEach(b => b.Floor.gameObject.SetActive(true));
        Bundles.ForEach(b => b.Abyss.gameObject.SetActive(false));
        Bundles.ForEach(b => { if (b.Walls != null) b.Walls.gameObject.SetActive(true); });
        Bundles.ForEach(b => b.Abyss.enabled = false);
        TimeLeft = TimeBetweenCollapses;
        index = 0;
    }

    int index = 0;
    float TimeLeft = 0;
    private void Start()
    {
        TimeLeft = TimeBetweenCollapses;
        index = 0;
    }
    public void SetTimeBetweenCollapses(float t)
    {
        TimeBetweenCollapses = t;
        ResetFloors();
    }
    IEnumerator Collapse(int bundleIndex)
    {
        if (bundleIndex >= Bundles.Count)
        {
            this.enabled = false;
            yield break;
        }
        float flashTimeLeft = FlashTime;
        Bundles[bundleIndex].Floor.gameObject.SetActive(false);
        if (Bundles[bundleIndex].Walls != null)
        {
            Bundles[bundleIndex].Walls.gameObject.SetActive(false);
        }
        while (flashTimeLeft > 0)
        {
            yield return new WaitForSeconds(0.1f);
            flashTimeLeft -= 0.1f;
            Bundles[bundleIndex].Floor.gameObject.SetActive(true);
            if (Bundles[bundleIndex].Walls != null)
            {
                Bundles[bundleIndex].Walls.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(0.1f);
            flashTimeLeft -= 0.1f;
            Bundles[bundleIndex].Floor.gameObject.SetActive(false);
            if (Bundles[bundleIndex].Walls != null)
            {
                Bundles[bundleIndex].Walls.gameObject.SetActive(false);
            }
        }

        yield return null;
        Bundles[bundleIndex].Floor.gameObject.SetActive(false);
        Bundles[bundleIndex].Abyss.gameObject.SetActive(true);
        if (Bundles[bundleIndex].Walls != null)
        {
            Bundles[bundleIndex].Walls.gameObject.SetActive(false);
        }

    }
    public bool TimerPaused { get; set; }
    // Update is called once per frame
    void Update () {
        if (TimerPaused) return;
        TimeLeft -= Time.deltaTime;
        while(TimeLeft <= 0)
        {
            Debug.Log("TIME OUT");
            TimeLeft += TimeBetweenCollapses;
            StartCoroutine(Collapse(index));
            index++;
        }
	}
}
