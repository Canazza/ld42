﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax : MonoBehaviour {
    public Transform Target;
    public Vector3 Scale;

    public Vector3 StartPosition;
	// Update is called once per frame
	void Update () {
        this.transform.position = Vector3.Scale(Target.position, Scale) + StartPosition;
	}
}
