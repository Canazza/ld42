﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class WeaponController : MonoBehaviour {
    [System.Serializable]
    public class WeaponBundle
    {
        public string Name;
        public int StartingAmmo;        
        public UnityEvent OnSelect, OnDeselect;
    }
    public WeaponBundle[] Weapons;
    public WeaponBundle CurrentWeapon;
    public float CurrentAmmo;
    private void Start()
    {
        SelectWeapon(Weapons[0].Name);
    }
    public void SelectWeapon(string name)
    {
        foreach (var weapon in Weapons) weapon.OnDeselect.Invoke();
        CurrentWeapon = Weapons.FirstOrDefault(x => x.Name.ToLower() == name.ToLower()) ?? CurrentWeapon;
        CurrentAmmo = CurrentWeapon.StartingAmmo;
        OnWeaponName.Invoke(CurrentWeapon.Name);
        WeaponActive = false;
        CurrentWeapon.OnSelect.Invoke();
    }
    public StringEvent OnAmmo, OnMaxAmmo, OnWeaponName;
    public bool WeaponActive { get; set; }
    public void AmmoUsed(float used)
    {
        CurrentAmmo -= used;
    }
    private void Update()
    {
        if(WeaponActive)
        {
            CurrentAmmo -= Time.deltaTime*10;
        }
        if (CurrentWeapon.StartingAmmo < 0)
        {
            OnAmmo.Invoke("-");
            OnMaxAmmo.Invoke("-");
        } else
        {
            if (CurrentAmmo <= 0)
            {
                SelectWeapon(Weapons[0].Name);
            }
            OnAmmo.Invoke(string.Format("{0:0}", CurrentAmmo));
            OnMaxAmmo.Invoke(string.Format("{0}", CurrentWeapon.StartingAmmo));
        }
    }

}
