﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class WaveSequenceController : MonoBehaviour {
    public WaveController[] Waves;
    public UnityEvent OnWaveComplete;
    public StringEvent OnWaveNumber;
    public UnityEvent OnLevelComplete;
    public float WaveDelay = 4;
    public int Lives = 3;
    public FloatEvent OnLives;
    public UnityEvent OnGameOver;
    public string NextLevelName; 
    public int WaveID;
    public void LoseLife()
    {
        Lives--;
        OnLives.Invoke(Lives / 3f);
        if (Lives < 0)
        {
            StartCoroutine(GameOverSequence());
        }
    }
    IEnumerator GameOverSequence()
    {
        OnGameOver.Invoke();
        yield return new WaitForSeconds(1);
        while (!Input.anyKeyDown)
        {
            yield return null;
        }
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }
    IEnumerator EndOfLevelSequence()
    {
        OnLevelComplete.Invoke();
        yield return new WaitForSeconds(1);
        while (!Input.anyKeyDown)
        {
            yield return null;
        }
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void NextWave()
    {
        WaveID++;
        OnWaveComplete.Invoke();
        StartCoroutine(StartWave(WaveID));
    }
	// Use this for initialization
	void Start () {
        Waves = this.GetComponentsInChildren<WaveController>();
        WaveID = 0;
        StartCoroutine(StartWave(WaveID));
        Lives = 3;

        OnLives.Invoke(1);
    }
    IEnumerator StartWave(int ID)
    {
        foreach (var wave in Waves)
        {
            wave.gameObject.SetActive(false);
        }
        if (ID >= Waves.Length)
        {
            StartCoroutine(EndOfLevelSequence());
            yield break;
        }
        yield return new WaitForSeconds(WaveDelay); 
        Waves[ID].gameObject.SetActive(true);
        Waves[ID].StartWave();
        OnWaveNumber.Invoke(string.Format("Wave {0} of {1}", ID + 1, Waves.Length));
    }

}
