﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

public class AudioLevelsController : MonoBehaviour {
    public AudioMixer mixer;
    public string SFXLevel = "SFX";
    public string MusicLevel = "Music";
    public FloatEvent OnSFXChange, OnMusicChange;
    public AnimationCurve VolumeCurve;
    public UnityEvent OnDelayedSFXChange;
    private float SFXLevelValue, MusicLevelValue;
	// Use this for initialization
    IEnumerator doDelayedSFXChange()
    {
        yield return new WaitForSeconds(0.2f);
        OnDelayedSFXChange.Invoke();
    }
	void Start ()
    {
        SFXLevelValue = PlayerPrefs.GetFloat(SFXLevel, 0.5f);
        MusicLevelValue = PlayerPrefs.GetFloat(MusicLevel, 0.5f);
        mixer.SetFloat(SFXLevel, VolumeCurve.Evaluate(SFXLevelValue));
        mixer.SetFloat(MusicLevel, VolumeCurve.Evaluate(MusicLevelValue));
        OnSFXChange.Invoke(PlayerPrefs.GetFloat(SFXLevel, 0.5f));
        OnMusicChange.Invoke(PlayerPrefs.GetFloat(MusicLevel, 0.5f));
    }
    public void SetSFXLevel(float level)
    {
        if (SFXLevelValue == level) return;
        Debug.Log("Set SFX Level " + level);
        SFXLevelValue = level;
        mixer.SetFloat(SFXLevel, VolumeCurve.Evaluate(level));
        PlayerPrefs.SetFloat(SFXLevel, level);
        PlayerPrefs.Save();
        StopAllCoroutines();
        StartCoroutine(doDelayedSFXChange());
        //OnSFXChange.Invoke(level);
    }
    public void SetMusicLevel(float level)
    {
        if (MusicLevelValue == level) return;
        Debug.Log("Set Music Level " + level);
        MusicLevelValue = level;
        mixer.SetFloat(MusicLevel, VolumeCurve.Evaluate(level));
        PlayerPrefs.SetFloat(MusicLevel, level);
        PlayerPrefs.Save();
        //OnMusicChange.Invoke(level);
    } 
}
