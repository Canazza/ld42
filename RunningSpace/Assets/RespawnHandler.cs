﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RespawnHandler : MonoBehaviour {
    public float DeathTime = 2;
    public float RespawnTime = 1;
    public UnityEvent OnDeathStart, OnDeathEnd, OnRespawnStart, OnRespawnEnd;
    public Transform Target;
    public void Respawn()
    {
        StartCoroutine(DoRespawn());
    }
    public IEnumerator DoRespawn()
    {
        OnDeathStart.Invoke();
        yield return new WaitForSeconds(DeathTime);
        OnDeathEnd.Invoke();
        yield return null;
        Target.position = this.transform.position;
        OnRespawnStart.Invoke();
        yield return new WaitForSeconds(RespawnTime);
        OnRespawnEnd.Invoke();
    }
}
