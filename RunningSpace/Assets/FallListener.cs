﻿using com.clydebuiltgames.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FallListener : MonoBehaviour, IDamageable {
    public UnityEvent OnFall;
    public bool DealDamage(int DamageAmount, GameObject source, DamageType sourceType = DamageType.None)
    {
        if (sourceType == DamageType.Falling)
        {
            OnFall.Invoke();
            return true;
        }
        return false;
    }
}
