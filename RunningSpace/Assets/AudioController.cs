﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
    public AudioSource source;
    public bool IsPlaying { get; set; }
	
	// Update is called once per frame
	void Update () {
		if(IsPlaying && !source.isPlaying)
        {
            source.Play();
        } else if(!IsPlaying && source.isPlaying)
        {
            source.Stop();
        }
	}
}
