﻿using com.clydebuiltgames.Controls;
using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadcopterAI : MonoBehaviour {
    public float Speed = 1;
    public float Acceleration = 1;
    public Transform DistanceTarget;
    public float Distance = 2;
    public float BackOffDistance = 4;
    public float JitterRadius = 1;
    public bool BackingOff;
    private float TargetDistance;
    private Vector2 Velocity, TargetVelocity;
    Vector3 TargetPosition;
    public BoolEvent IsAdvancing; 
    private void OnEnable()
    {
        if (DistanceTarget == null)
        {
            DistanceTarget = FindObjectOfType<TopDownController>().transform;
        }
        StartCoroutine(MoveAI());
    }
    IEnumerator MoveAI()
    {
        while (true)
        {


            TargetDistance = (BackingOff ? BackOffDistance : Distance) + Random.Range(-JitterRadius, JitterRadius);
            
            while (!MoveTowardsDistance(TargetDistance))
            {
                yield return null;
            }
            BackingOff = !BackingOff;
            IsAdvancing.Invoke(!BackingOff);
            yield return null;
        }
    }
    private void FixedUpdate()
    {
        this.transform.position += (Vector3)(Velocity * Time.fixedDeltaTime);
    }
    bool MoveTowardsDistance(float d)
    {
        TargetPosition = (this.transform.position - DistanceTarget.position).normalized * d;
        TargetVelocity = (DistanceTarget.position + TargetPosition - this.transform.position).normalized * Speed;
        Velocity = Vector2.MoveTowards(Velocity, TargetVelocity, Time.deltaTime * Acceleration);
        //this.transform.position = Vector3.MoveTowards(this.transform.position, DistanceTarget.position + target, Time.deltaTime * Speed );
        return Vector2.Distance(this.transform.position, DistanceTarget.position + TargetPosition) < 0.1f;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(DistanceTarget.position, Distance);
        Gizmos.DrawWireSphere(DistanceTarget.position, BackOffDistance);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(DistanceTarget.position, TargetDistance);
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(this.transform.position, this.transform.position + (Vector3) TargetVelocity);
        Gizmos.DrawSphere(DistanceTarget.position + TargetPosition, 0.1f);
    }
}
