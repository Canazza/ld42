﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.clydebuiltgames.AudioSync
{
    public class AudioSyncController : MonoBehaviour
    {
        public List<Prominence> prominences;
        private List<AudioSyncMonitor> Monitors;
        private List<AudioSyncOutput> Outputs;
        public int[] ProminenceValues = new int[] { 100, 70, 50, 30, 20, 10, 5, 2 };
        public int BPM = 120;
        public bool StartOnAwake = true;
        private void Awake()
        {
            if (StartOnAwake) BeginSyncing();
        }
        void BeginSyncing()
        {
            Monitors = prominences.Select(x => x.Monitor).ToList();
            Outputs = prominences.Select(x => x.Output).ToList();
            StartCoroutine(SyncWorker());
        }
        IEnumerator SyncWorker()
        {
            float timeBetweenUpdates = BPM / 60f;
            float nextUpdateTime = timeBetweenUpdates;
            float actualTimePassed = 0;
            UpdateProminences();
            FireProminenceEvents(true);
            while (true)
            {
                actualTimePassed = 0;
                for(float t = 0; t < nextUpdateTime; t += Time.deltaTime)
                {
                    yield return null;
                    actualTimePassed += Time.deltaTime;
                }
                nextUpdateTime = timeBetweenUpdates + (timeBetweenUpdates - actualTimePassed); 
                UpdateProminences();
                FireProminenceEvents(false);
                
            }
        } 
        void FireProminenceEvents(bool instant)
        {
            foreach(var prominence in prominences)
            {
                prominence.FireEvents(instant);
            }
        }
        void UpdateProminences()
        {
            var monitorOrder = Monitors.Where(m=>m.Value > 0).OrderBy(m => m.Value).ToList();
            foreach (var prominence in prominences)
            {
                int index = monitorOrder.IndexOf(prominence.Monitor);
                if (index >= 0 && index < ProminenceValues.Length)
                { 
                    prominence.Value = ProminenceValues[index] / 100f;  
                } else
                {
                    prominence.Value = 0;
                }
            } 

        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}