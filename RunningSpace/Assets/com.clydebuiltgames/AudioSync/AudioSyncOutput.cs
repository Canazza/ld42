﻿using com.clydebuiltgames.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace com.clydebuiltgames.AudioSync
{
    public class AudioSyncOutput : MonoBehaviour
    {
        public FloatEvent OnVolumeChange;
        public AnimationCurve VolumeCurve;
        public float FadeTime = 0.2f;
        public float Value = 0;
        public void SetVolume(float t, bool instant = false)
        {
            if (Value != t)
            {
                if (instant)
                {
                    OnVolumeChange.Invoke(VolumeCurve.Evaluate(t));
                }
                else
                {
                    StartCoroutine(DoSetVolume(Value, t));
                }
            }
            Value = t;
        }
        private IEnumerator DoSetVolume(float from, float target)
        {
            for (float t = 0; t < 1; t += Time.deltaTime / FadeTime)
            {
                OnVolumeChange.Invoke(VolumeCurve.Evaluate(Mathf.Lerp(from,target,t)));
                yield return null;
            }
            OnVolumeChange.Invoke(VolumeCurve.Evaluate(target));
        }
    }

}