﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.AudioSync
{
    [System.Serializable]
    public class Prominence
    {
        public AudioSyncMonitor Monitor;
        public AudioSyncOutput Output;
        public AnimationCurve ProminenceCurve;
        public float Value = 0; 
        public void FireEvents(bool instant = false)
        {
            Output.SetVolume(ProminenceCurve.Evaluate(Value), instant);
        }
    } 
}
