﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.AudioSync
{
    public class AudioSyncMonitor : MonoBehaviour
    {
        [Range(0, 1)]
        public float Value;

        public void SetProminence(float v)
        {
            Value = v;
        }
    }
}