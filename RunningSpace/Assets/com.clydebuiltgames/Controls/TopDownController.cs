﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Controls
{
    public class TopDownController : MonoBehaviour
    {
        public Vector2 MoveDirection;
        public float MoveSpeed = 1;
        public Transform RotationRoot;
        public float ColliderSize = 0.5f;
        public LayerMask ColliderMask;
        public float KnockbackSpeed = 1;
        public float KnockbackDuration = 0.2f;
        public Animator animator;
        public string AnimationWalkingBool = "Walking";
        public bool LockAim { get; set; }
        public void SetX(int x)
        {
            MoveDirection.x = Mathf.Clamp(x, -1, 1);
        }
        public void SetY(int y)
        {
            MoveDirection.y = Mathf.Clamp(y, -1, 1);
        } 
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireCube(this.transform.position, new Vector3(ColliderSize * 2, ColliderSize * 2, 0));
        }
        private void Update()
        {
            animator.SetBool(AnimationWalkingBool, MoveDirection.magnitude > 0);
        }
        private void FixedUpdate()
        {
            this.transform.Translate(MoveDirection.normalized * MoveSpeed * Time.deltaTime);
            if (MoveDirection.magnitude > 0 && !LockAim) RotationRoot.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(MoveDirection.y, MoveDirection.x) * Mathf.Rad2Deg);
            Test(Vector2.left);
            Test(Vector2.right);
            Test(Vector2.up);
            Test(Vector2.down);
        }
        void Test(Vector2 direction)
        {

            var hit = Physics2D.Raycast(this.transform.position, direction, ColliderSize, ColliderMask);
            if (hit.collider != null)
            { 
                this.transform.position = hit.point - direction * ColliderSize; 
            }

        }
        public void KnockBack(Vector2 Source)
        {
            StartCoroutine(DoKnockBack(Source));
        }
        private IEnumerator DoKnockBack(Vector2 Source)
        {
            if (KnockbackDuration <= 0) yield break;
            var direction = ( (Vector2)this.transform.position - Source).normalized; // - MoveDirection.normalized;
            for(float t = 0; t < 1; t += Time.deltaTime / KnockbackDuration)
            {
                yield return null;
                this.transform.Translate(direction * Time.deltaTime * KnockbackSpeed);
            }
        }
    }

}