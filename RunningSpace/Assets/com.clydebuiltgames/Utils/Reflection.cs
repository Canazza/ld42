﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Utils
{
    public static class Reflection
    {
        public static FieldInfo GetComponentFieldInfo(this GameObject gameObject, string path)
        {
            var bits = path.Split('.');
            var component = gameObject.GetComponent(bits[0]);
            return component.GetType().GetField(bits[1]);
        } 
    }
}
