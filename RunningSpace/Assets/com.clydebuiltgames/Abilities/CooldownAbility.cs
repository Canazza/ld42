﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Abilities
{
    public class CooldownAbility : MonoBehaviour
    {
        public float CooldownTime = 1;
        private float TimeLeft = 0;
        public UnityEvent OnFire;
        public bool AbilityIsActive { get; set; }
        private void Update()
        {
            if (TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
            }
            if(AbilityIsActive && TimeLeft <= 0)
            {
                TimeLeft = CooldownTime;
                OnFire.Invoke();
            }
        }
    }

}