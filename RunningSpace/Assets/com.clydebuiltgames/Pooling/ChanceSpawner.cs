﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Pooling
{
    public class ChanceSpawner:MonoBehaviour
    {
        [System.Serializable]
        public class ChanceBundle
        {
            public GameObject Prefab;
            public int Chance;
        }
        public List<ChanceBundle> Prefabs;
        public Transform Parent;
        public Transform Orientation;
        public Transform Position;
        private void Start()
        {
            if (Orientation == null) Orientation = this.transform;
            if (Position == null) Position = this.transform;
        }
        private GameObject Roll()
        {
            var max = Prefabs.Sum(x => x.Chance);
            var roll = UnityEngine.Random.Range(0, max);
            foreach(var p in Prefabs)
            {
                roll -= p.Chance;
                if (roll < 0) return p.Prefab;
            }
            return null;
        }
        public void Spawn()
        {
            var Prefab = Roll();
            if (Prefab == null) return;
            GameObject instance = PoolTracker.Get(Prefab);
            instance.transform.SetParent(Parent);
            instance.transform.position = Position.position;
            instance.transform.rotation = Orientation.rotation;
            instance.SetActive(true);
        }
    }
}
