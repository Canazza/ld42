﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Pooling
{
    public class Spawner:MonoBehaviour
    {
        public GameObject Prefab;
        public Transform Parent;
        public Transform Orientation;
        public Transform Position;
        private void Start()
        {
            if (Orientation == null) Orientation = this.transform;
            if (Position == null) Position = this.transform;
        }
        public void Spawn()
        {
            GetSpawn();
        }
            public GameObject GetSpawn()
        {
            if (Orientation == null) Orientation = this.transform;
            if (Position == null) Position = this.transform;
            GameObject instance = PoolTracker.Get(Prefab);
            instance.transform.SetParent(Parent);
            instance.transform.position = Position.position;
            instance.transform.rotation = Orientation.rotation;
            instance.SetActive(true);
            return instance;
        }
    }
}
