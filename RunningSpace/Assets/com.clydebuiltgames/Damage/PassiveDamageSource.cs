﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Damage
{
    public class PassiveDamageSource : MonoBehaviour, ICollidable
    {
        public int DamageToDeal;
        public DamageType DamageType = DamageType.None;
        public void CollidedWith(GameObject source)
        {
            var damageables = source.GetComponents<IDamageable>();
            var wasHit = false;
            foreach (var damageable in damageables)
            {
                if (damageable != null)
                {
                    damageable.DealDamage(DamageToDeal, this.gameObject, DamageType);
                    wasHit = true;
                }
            }
            if(wasHit)
            {
                OnHit.Invoke();
            }
        }
        public UnityEvent OnHit;
    }
}
