﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Damage
{
    public class HealthPickup : MonoBehaviour, ICollidable
    {
        public int HealthToHeal;
        public UnityEvent OnPickup;
        public SpriteRenderer Sprite;
        public float WaitBeforeFlash = 5;
        public float FlashDuration = 4;
        void OnEnable()
        {
            Sprite.enabled = true;
            StartCoroutine(DoDestroyTimer());
        }
        private IEnumerator DoDestroyTimer()
        {
            yield return new WaitForSeconds(WaitBeforeFlash);
            StartCoroutine(Flash());
            yield return new WaitForSeconds(FlashDuration);
            this.gameObject.SetActive(false);
        }
        private IEnumerator Flash()
        {
            while (true)
            {
                Sprite.enabled = true;
                yield return new WaitForSeconds(0.1f);
                Sprite.enabled = false;
                yield return new WaitForSeconds(0.1f);

            }
        }
        public void CollidedWith(GameObject source)
        {
            var healable = source.GetComponent<IHealable>();
            if (healable != null)
            {
                if (healable.HealDamage(HealthToHeal, this.gameObject, DamageType.Healing))
                {
                    OnPickup.Invoke();
                }
            }
        }
    }
}
