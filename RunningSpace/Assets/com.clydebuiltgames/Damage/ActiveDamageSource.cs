﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.clydebuiltgames.Damage
{
    public class ActiveDamageSource : MonoBehaviour
    {
        public int Damage;
        public DamageType DamageType;
        public Collider2D Collider;
        public ContactFilter2D Filter;
        private Collider2D[] Results = new Collider2D[10];
        public UnityEvent OnHit;
        private void Update()
        {
            Check();
        }
        private void Check()
        {
            int found = Physics2D.OverlapCollider(Collider, Filter, Results);
            if (found > 0)
            {
                for (int i = 0; i < found; i++)
                {
                    var damageable = Results[i].GetComponent<IDamageable>();
                    if (damageable != null)
                    {
                        damageable.DealDamage(Damage, this.gameObject, DamageType);
                    }
                }
                OnHit.Invoke();
            }
        }
    }
}