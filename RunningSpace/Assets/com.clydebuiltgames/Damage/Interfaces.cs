﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.clydebuiltgames.Damage
{
    public enum DamageType
    {
        None, Piercing, Crushing, Fire, Electrical, Slashing, Cold, Healing, Falling
    }
    public interface IDamageable
    {
        bool DealDamage(int DamageAmount, GameObject source, DamageType sourceType =  DamageType.None); 
    }
    public interface IHealable
    {
        bool HealDamage(int HealAmount, GameObject source, DamageType sourceType = DamageType.None);

    } 
    public interface ICollidable
    {
        void CollidedWith(GameObject source);
    }
}