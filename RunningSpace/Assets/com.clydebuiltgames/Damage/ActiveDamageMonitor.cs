﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace com.clydebuiltgames.Damage
{
    public class ActiveDamageMonitor:MonoBehaviour
    {
        public Collider2D Collider;
        public ContactFilter2D Filter;
        private Collider2D[] Results = new Collider2D[10];
        private void Update()
        {
            Check();
        }
        private void Check()
        {
            int found = Physics2D.OverlapCollider(Collider, Filter, Results);
            if(found > 0)
            {
                for(int i = 0; i < found; i++)
                {
                    var collidable = Results[i].GetComponent<ICollidable>();
                    if(collidable != null)
                    { 
                        collidable.CollidedWith(this.gameObject);
                    }
                }
            }
        }
    }
}
