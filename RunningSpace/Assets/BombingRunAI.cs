﻿using com.clydebuiltgames.Controls;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombingRunAI : MonoBehaviour {
    public Transform Target;
    public float LineLength;
    public GameObject Prefab; 
    public Vector2[] GetPositions()
    {
        var direction = (this.transform.position - Target.position).normalized * LineLength;
        var position = Target.position;
        List<Vector2> points = new List<Vector2>();
        for (float t = -1; t <= 1; t += 0.5f)
        {
            points.Add(direction * t + position);
        }
        return points.ToArray();
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        foreach(var point in GetPositions())
        {
            Gizmos.DrawWireSphere(point, 0.5f);
        }
    }
    // Use this for initialization
    void Start () {
        if (Target == null)
        {
            Target = FindObjectOfType<TopDownController>().transform;
        } 

    }
	IEnumerator SpawnItems()
    {
        Debug.Log("SPAWN ITEMS");
        this.transform.position = Quaternion.Euler(0, 0, Random.Range(0, 360)) * Vector2.up + Target.position;
        foreach(var point in GetPositions())
        {
            var p = com.clydebuiltgames.Pooling.PoolTracker.Get(Prefab);
            p.transform.position = point;
            p.gameObject.SetActive(true);
            yield return new WaitForSeconds(TimeBetweenSpawn);
        }
    }
    public float Cooldown = 3;
    public float CooldownRemaining = 3;
    public float TimeBetweenSpawn = 0.1f;
	// Update is called once per frame
	void Update () {
        if (CooldownRemaining > 0)
        {
            CooldownRemaining -= Time.deltaTime;
        } else
        {
            CooldownRemaining = Cooldown;
            StartCoroutine(SpawnItems());
        }
	}
}
